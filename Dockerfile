FROM openjdk:18
ARG JAR_FILE=EuropaceProject-0.0.1-SNAPSHOT.jar
COPY /target/${JAR_FILE} EuropaceProject-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/EuropaceProject-0.0.1-SNAPSHOT.jar"]

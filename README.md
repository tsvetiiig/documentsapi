# Europace Project Document API

Europace Documents API, which retrieves and filters the information received by the Documents API

### Requirements
* Java 18
* Docker

### How to run

1. Build the project:  
`./mvn clean install`

2. Create the docker image:  
`docker build -t europace-project/documents-api .`

3. Run the docker image:  
`docker run -d -p 8081:8081 europace-project/documents-api`

## REST API Endpoints
Provided endpoints:

- ```/api/v1/documents/``` - retrieves all documents
- ```/api/v1/documents/number``` - retrieves the total number of documents
- ```/api/v1/documents/number/deleted``` - retrieves the number of deleted documents
- ```/api/v1/documents/total_size``` - retrieves the total size of all documents
- ```/api/v1/documents/average_size``` - retrieves the average size of the documents
- ```/api/v1/documents/search?categories={category1,category2}``` - retrieves a list of documents with the specified categories
- ```/api/v1/documents/?sort={sort_property}&order={order_property}``` - retrieves a list of documents sorted by the specified property and order.
The properties to sort by are: id, name, size, deleted, createdAt and modifiedAt. The valid values for order are: ASC and DESC.
The sort will be by ascending order if not specified.


Example request:
```
curl --location --request GET 'http://localhost:8081/api/v1/documents/'
```

Example response:
```
[
    {
        "id": "1",
        "name": "document name",
        "size": 10540,
        "type": "PDF",
        "categories": [
            "cat_1",
            "cat_2"
        ],
        "deleted": false,
        "createdAt": "2021-10-16T10:14:41.595",
        "modifiedAt": "2021-10-16T10:14:41.595"
    },
    {
        "id": "2",
        "name": "document name2",
        "size": 10542,
        "type": "PDF",
        "categories": [
            "cat_3"
        ],
        "deleted": true,
        "createdAt": "2021-11-16T10:14:41.595",
        "modifiedAt": "2021-11-16T10:14:41.595"
    }
]
```

Example request:
```
curl --location --request GET 'http://localhost:8081/api/v1/documents/number'
```

Example response:
```
10
```




package com.europace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuropaceProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EuropaceProjectApplication.class, args);
    }

}

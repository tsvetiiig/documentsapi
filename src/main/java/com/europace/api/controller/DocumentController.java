package com.europace.api.controller;

import com.europace.api.domain.Document;
import com.europace.api.service.IDocumentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/documents")
@AllArgsConstructor
public class DocumentController {
    private final IDocumentService documentService;

    @CrossOrigin
    @GetMapping("/")
    public ResponseEntity<List<Document>> getDocuments() {
        return ResponseEntity.ok(documentService.getAllDocuments());
    }

    @CrossOrigin
    @GetMapping("/number")
    public ResponseEntity<Integer> getNumberOfDocuments() {
        return ResponseEntity.ok(documentService.getNumberOfDocuments());
    }

    @CrossOrigin
    @GetMapping("/number/deleted")
    public ResponseEntity<Integer> getNumberOfDeletedDocuments() {
        return ResponseEntity.ok(documentService.getNumberOfDeletedDocuments());
    }

    @CrossOrigin
    @GetMapping("/total_size")
    public ResponseEntity<Long> getTotalSizeOfDocuments() {
        return ResponseEntity.ok(documentService.getTotalSizeOfDocuments());
    }

    @CrossOrigin
    @GetMapping("/average_size")
    public ResponseEntity<Double> getAverageSizeOfDocuments() {
        return ResponseEntity.ok(documentService.getAverageSizeOfDocuments());
    }

    @CrossOrigin
    @GetMapping("/search")
    public ResponseEntity<List<Document>> filterByCategories(@RequestParam("categories") List<String> categories) {
        return ResponseEntity.ok(documentService.filterByCategories(categories));
    }

    @CrossOrigin
    @GetMapping()
    public ResponseEntity<List<Document>> sortBy(@RequestParam("sort") String property,
                                                 @RequestParam(defaultValue = "ASC") String order) {
        return ResponseEntity.ok(documentService.sortBy(property, order));
    }
}

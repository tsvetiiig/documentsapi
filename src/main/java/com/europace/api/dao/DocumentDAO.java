package com.europace.api.dao;

import com.europace.api.domain.Document;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.ConnectException;
import java.util.List;

@Service
public class DocumentDAO implements IDocumentDAO {

    private static final String documentsResourceUrl = "http://localhost:8080/v1/documents";
    private final RestTemplate restTemplate;
    private final HttpEntity<String> requestEntity;

    public DocumentDAO(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .build();

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        this.requestEntity = new HttpEntity<>(headers);
    }

    @Override
    public List<Document> getDocuments() {
        try {
            ResponseEntity<List<Document>> responseEntity = restTemplate.exchange(
                    documentsResourceUrl, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<>() {
                    });

            return responseEntity.getBody();
        } catch (RestClientException e) {
            if (e.getCause() instanceof ConnectException) {
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, "Requested URL cannot be accessed: " + documentsResourceUrl);
            }
            else throw e;
        }
    }
}

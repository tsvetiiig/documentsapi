package com.europace.api.dao;

import com.europace.api.domain.Document;

import java.util.List;

public interface IDocumentDAO {

    /**
     * Gets all documents.
     *
     * @return list of {@link Document}
     */
    List<Document> getDocuments();
}

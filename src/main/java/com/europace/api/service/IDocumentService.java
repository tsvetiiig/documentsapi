package com.europace.api.service;

import com.europace.api.domain.Document;

import java.util.List;

public interface IDocumentService {

    /**
     * Gets all documents.
     *
     * @return list of {@link Document}
     */
    List<Document> getAllDocuments();

    /**
     * Gets the number of all documents.
     *
     * @return total number of documents
     */
    int getNumberOfDocuments();

    /**
     * Gets the number of deleted documents.
     *
     * @return number of deleted documents
     */
    int getNumberOfDeletedDocuments();

    /**
     * Gets the total size of all documents.
     *
     * @return the total documents size
     */
    long getTotalSizeOfDocuments();

    /**
     * Gets the average size of the documents.
     *
     * @return the average documents size
     */
    double getAverageSizeOfDocuments();

    /**
     * Gets only the documents that has all the specified categories.
     *
     * @param categories the categories by which the documents are filtered
     * @return list of documents that match the filter criteria
     */
    List<Document> filterByCategories(List<String> categories);

    /**
     * Gets the documents sorted by a given property in an ascending or descending order.
     *
     * @param property the property to sort by
     * @param order the order to sort - ASC or DESC
     * @return list of documents sorted by the given property and order
     */
    List<Document> sortBy(String property, String order);
}

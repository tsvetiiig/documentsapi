package com.europace.api.service;

import com.europace.api.domain.Document;
import com.europace.api.dao.IDocumentDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class DocumentService implements IDocumentService {

    private static final String ASCENDING_ORDER = "ASC";
    private static final String DESCENDING_ORDER = "DESC";
    private static final Map<String, Comparator<Document>> sortPropertiesComparators = Map.of(
            "id", Comparator.comparing(Document::getId),
            "name", Comparator.comparing(Document::getName),
            "size", Comparator.comparingLong(Document::getSize),
            "deleted", Comparator.comparing(Document::getDeleted),
            "createdAt", Comparator.comparing(Document::getCreatedAt),
            "modifiedAt", Comparator.comparing(Document::getModifiedAt)
    );

    private final IDocumentDAO documentProvider;

    @Override
    public List<Document> getAllDocuments() {
        return documentProvider.getDocuments();
    }

    @Override
    public int getNumberOfDocuments() {
        return documentProvider.getDocuments().size();
    }

    @Override
    public int getNumberOfDeletedDocuments() {
        return (int) documentProvider.getDocuments().stream().filter(Document::getDeleted).count();
    }

    @Override
    public long getTotalSizeOfDocuments() {
        return documentProvider.getDocuments().stream().map(Document::getSize).reduce(0L, Long::sum);
    }

    @Override
    public double getAverageSizeOfDocuments() {
        if (getNumberOfDocuments() == 0) {
            return 0;
        }
        return (double) getTotalSizeOfDocuments() / getNumberOfDocuments();
    }

    @Override
    public List<Document> filterByCategories(List<String> categories) {
        Stream<Document> documentStream = documentProvider.getDocuments().stream();
        for (String category : categories) {
            documentStream = documentStream.filter(document -> document.getCategories().contains(category));
        }
        return documentStream.toList();
    }

    @Override
    public List<Document> sortBy(String property, String order) {
        if (!sortPropertiesComparators.containsKey(property)) {
            throw new IllegalArgumentException("The valid sort properties are: id, name, size, deleted, createdAt and modifiedAt.");
        }
        if (!ASCENDING_ORDER.equalsIgnoreCase(order) && !DESCENDING_ORDER.equalsIgnoreCase(order)) {
            throw new IllegalArgumentException("The valid sort order value must be: ASC or DESC.");
        }
        return documentProvider.getDocuments().stream().sorted(getComparator(property, order)).toList();
    }

    private Comparator<Document> getComparator(String propertyName, String order) {
        Comparator<Document> comparator = sortPropertiesComparators.get(propertyName);
        if (ASCENDING_ORDER.equalsIgnoreCase(order)) {
            return comparator;
        }
        return comparator.reversed();
    }
}

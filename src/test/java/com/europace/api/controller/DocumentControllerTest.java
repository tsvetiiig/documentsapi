package com.europace.api.controller;

import com.europace.api.domain.Document;
import com.europace.api.service.IDocumentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class DocumentControllerTest {

    @InjectMocks
    DocumentController documentController;

    @Mock
    IDocumentService documentService;

    private static final Document document1 = new Document("1", "ImageTest", 1L, Document.Type.IMAGE,
            List.of("cat_1", "cat_2"), Boolean.FALSE, LocalDateTime.now(), LocalDateTime.now());
    private static final Document document2 = new Document("2", "PdfTest", 2L, Document.Type.PDF,
            List.of("cat_2"), Boolean.TRUE, LocalDateTime.now(), LocalDateTime.now());

    @Test
    void testGetDocumentsSuccess() {
        List<Document> documents = List.of(document1, document2);
        Mockito.when(documentService.getAllDocuments()).thenReturn(documents);

        ResponseEntity<List<Document>> responseEntity = documentController.getDocuments();

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(2);
        assertThat(responseEntity.getBody().get(0)).isEqualTo(document1);
    }

    @Test
    void testGetNumberOfDocumentsSuccess() {
        Mockito.when(documentService.getNumberOfDocuments()).thenReturn(2);

        ResponseEntity<Integer> responseEntity = documentController.getNumberOfDocuments();

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).isEqualTo(2);
    }

    @Test
    void testGetNumberOfDeletedDocumentsSuccess() {
        Mockito.when(documentService.getNumberOfDeletedDocuments()).thenReturn(1);

        ResponseEntity<Integer> responseEntity = documentController.getNumberOfDeletedDocuments();

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).isEqualTo(1);
    }

    @Test
    void testGetTotalSizeOfDocumentsSuccess() {
        Mockito.when(documentService.getTotalSizeOfDocuments()).thenReturn(2L);

        ResponseEntity<Long> responseEntity = documentController.getTotalSizeOfDocuments();

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).isEqualTo(2L);
    }

    @Test
    void testGetAverageSizeOfDocumentsSuccess() {
        Mockito.when(documentService.getAverageSizeOfDocuments()).thenReturn(2.5);

        ResponseEntity<Double> responseEntity = documentController.getAverageSizeOfDocuments();

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).isEqualTo(2.5);
    }

    @Test
    void testFilterByCategoriesSuccess() {
        List<String> categories = Collections.singletonList("cat_2");
        Mockito.when(documentService.filterByCategories(categories))
                .thenReturn(Collections.singletonList(document2));

        ResponseEntity<List<Document>> responseEntity = documentController.filterByCategories(categories);

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(1);
        assertThat(responseEntity.getBody().get(0)).isEqualTo(document2);
    }

    @Test
    void testSortBySuccess() {
        List<Document> reversedOrderedDocuments = List.of(document2, document1);
        Mockito.when(documentService.sortBy("id", "DESC")).thenReturn(reversedOrderedDocuments);

        ResponseEntity<List<Document>> responseEntity = documentController.sortBy("id", "DESC");

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(2);
        assertThat(responseEntity.getBody().get(0).getId()).isEqualTo("2");
    }
}
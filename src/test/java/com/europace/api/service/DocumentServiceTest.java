package com.europace.api.service;

import com.europace.api.dao.IDocumentDAO;
import com.europace.api.domain.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class DocumentServiceTest {

    private static final Document document1 = new Document("1", "ImageTest", 1L, Document.Type.IMAGE,
            List.of("cat_1", "cat_2"), Boolean.FALSE, LocalDateTime.now(), LocalDateTime.now());
    private static final Document document2 = new Document("2", "PdfTest", 2L, Document.Type.PDF,
            List.of("cat_2"), Boolean.TRUE, LocalDateTime.now(), LocalDateTime.now());
    private static final List<Document> documents = List.of(document1, document2);

    @InjectMocks
    DocumentService documentService;

    @Mock
    IDocumentDAO documentDAO;

    @Test
    void testGetAllDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> documents = documentService.getAllDocuments();

        assertThat(documents.size()).isEqualTo(2);
        assertThat(documents.get(0)).isEqualTo(document1);
        assertThat(documents.get(1)).isEqualTo(document2);
    }

    @Test
    void testGetTotalNumberOfDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        assertThat(documentService.getNumberOfDocuments()).isEqualTo(2);
    }

    @Test
    void testGetNumberOfDeletedDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        assertThat(documentService.getNumberOfDeletedDocuments()).isEqualTo(1);
    }

    @Test
    void testGetTotalSizeOfDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        assertThat(documentService.getTotalSizeOfDocuments()).isEqualTo(3);
    }

    @Test
    void testGetAverageSizeOfDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        assertThat(documentService.getAverageSizeOfDocuments()).isEqualTo(1.5);
    }

    @Test
    void testGetAverageSizeOfNoDocuments() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(Collections.emptyList());

        assertThat(documentService.getAverageSizeOfDocuments()).isEqualTo(0);
    }

    @Test
    void testFilterByOneCategory() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> filteredDocuments1 = documentService.filterByCategories(Collections.singletonList("cat_1"));
        List<Document> filteredDocuments2 = documentService.filterByCategories(Collections.singletonList("cat_2"));

        assertThat(filteredDocuments1.size()).isEqualTo(1);
        assertThat(filteredDocuments1.get(0)).isEqualTo(document1);

        assertThat(filteredDocuments2.size()).isEqualTo(2);
        assertThat(filteredDocuments2.get(0)).isEqualTo(document1);
        assertThat(filteredDocuments2.get(1)).isEqualTo(document2);
    }

    @Test
    void testFilterByTwoCategories() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> filteredDocuments = documentService.filterByCategories(List.of("cat_2", "cat_1"));

        assertThat(filteredDocuments.size()).isEqualTo(1);
        assertThat(filteredDocuments.get(0)).isEqualTo(document1);
    }

    @Test
    void testFilterByNotMatchingCategory() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> filteredDocuments = documentService.filterByCategories(List.of("cat_not_existing"));

        assertThat(filteredDocuments.size()).isEqualTo(0);
    }

    @Test
    void testSortByNameAscending() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> sortedDocuments = documentService.sortBy("name", "ASC");

        assertThat(sortedDocuments.size()).isEqualTo(2);
        assertThat(sortedDocuments.get(0)).isEqualTo(document1);
        assertThat(sortedDocuments.get(1)).isEqualTo(document2);
    }

    @Test
    void testSortByIdDescending() {
        Mockito.when(documentDAO.getDocuments()).thenReturn(documents);

        List<Document> sortedDocuments = documentService.sortBy("id", "DESC");

        assertThat(sortedDocuments.size()).isEqualTo(2);
        assertThat(sortedDocuments.get(0)).isEqualTo(document2);
        assertThat(sortedDocuments.get(1)).isEqualTo(document1);
    }

    @Test
    void testSortByInvalidProperty() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            documentService.sortBy("invalid_property", "DESC");
        });

        Assertions.assertEquals("The valid sort properties are: id, name, size, deleted, createdAt and modifiedAt.",
                exception.getMessage());
    }

    @Test
    void testSortByInvalidOrder() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            documentService.sortBy("id", "invalid_order");
        });

        Assertions.assertEquals("The valid sort order value must be: ASC or DESC.", exception.getMessage());
    }
}